﻿using System;

namespace Properties {

    public class Card
    {
        private readonly string seed;
        private readonly string name;
        private readonly int ordial;

        public Card(string name, string seed, int ordial)
        {
            this.name = name;
            this.ordial = ordial;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        // TODO improve
        public string Seed
        {
            get => seed;
        }

        // TODO improve
        public string Name
        {
            get => name;
        }

        // TODO improve
        public int Ordinal
        {
            get =>ordial;
        }

        public override string ToString()
        {
            // TODO understand string interpolation
            return $"{this.GetType().Name}(Name={this.Name}, Seed={this.Seed}, Ordinal={this.Ordinal})";
        }

        public override bool Equals(object obj)
        {
            // TODO improve
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            // TODO improve
            return ((ordial * 531) +17)%9;
        }
    }

}